package com.example.flickrapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    private Button buttonGetAnImage;
    private ImageView image;
    private Button buttonGoToListView;

    private String URLImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonGetAnImage = findViewById(R.id.buttonGetAnImage);
        image = findViewById(R.id.image);
        buttonGoToListView = findViewById(R.id.buttonToListActivity);

        //When the user click on the button Get An Image
        buttonGetAnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //When the button is clicked, we start a new AsynTask of type AsyncFlickrJSONData
                AsyncFlickrJSONData data = new AsyncFlickrJSONData();
                data.execute("https://www.flickr.com/services/feeds/photos_public.gne?tags=trees&format=json");
            }
        });

        Intent intentListView = new Intent(this, ListActivity.class);

        buttonGoToListView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //When the button is clicked, we start the activity ListActivity
                startActivity(intentListView);
            }
        });

    }

    //A class that extends Asynctask, s
    private class AsyncFlickrJSONData extends AsyncTask<String, Void, JSONObject>{

        @Override
        protected JSONObject doInBackground(String... strings) {
            URL url = null;
            String s = "";
            JSONObject jsonobj = null;
            try {
                url = new URL(strings[0]);
                HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                try {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    s = readStream(in);
                    Log.i("JFL", s);
                    try {
                        int debut = s.indexOf("(");
                        int fin = s.lastIndexOf(")");
                        String newJsonFormat = s.substring(debut + 1, fin);
                        jsonobj = new JSONObject(newJsonFormat);
                    } catch (JSONException err){
                        Log.d("Error", err.toString());
                    }
                } finally {
                    urlConnection.disconnect();
                }
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(jsonobj);
            return jsonobj;
        }

        //In this method we manipulate the JSONObject
        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            try {
                //We use getJSONArray to get an Array of the items in the JSONObject
                URLImage = jsonObject.getJSONArray("items").getJSONObject(1).getString("media");
                int debut = URLImage.indexOf("https");
                int fin = URLImage.lastIndexOf("g");
                URLImage = URLImage.substring(debut, fin + 1 );
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //Here we create an object AsynBitmapDownloader
            AsyncBitmapDownloader DownloadBitMap = new AsyncBitmapDownloader();
            //Then we can download the image who is at this URL
            DownloadBitMap.execute(URLImage);
        }

        private String readStream(InputStream in) {
            try {
                ByteArrayOutputStream bo = new ByteArrayOutputStream();
                int i = in.read();
                while(i != -1) {
                    bo.write(i);
                    i = in.read();
                }
                return bo.toString();
            } catch (IOException e) {
                return "";
            }
        }


    }

    //This class extends from AsyncTask and will be used to download an image given its input URL.
    private class AsyncBitmapDownloader extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... strings) {
            URL url = null;
            Bitmap Render = null;
            try {
                url = new URL(strings[0]);
                HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                Bitmap bm = BitmapFactory.decodeStream(in);
                Render = bm;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return Render;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    image.setImageBitmap(bitmap);
                }
            });
        }

    }


}

